const mongodb = require("mongodb");
const mongoose = require("mongoose");
const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cross = require("./cross.js");
const session = require('express-session');
const Cookies = require('cookies')

const PORT = 5000;

const app = express();
app.use(bodyParser.json());
app.use(cross);
app.use(cookieParser());

let keys = ['keyboard cat']
let CSRF_bucket = [];

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: true }
}))

app.get ('/test',(req,res)=>{
  var cookies = new Cookies(req, res, { keys: keys });
  // genrate cookie with sission ID
  cookies.set('sessionID', req.sessionID, {  });
  //geanrate cookie with username
  cookies.set('username', 'admin', {  });
  //genarate CSRD Token
  let CSRF = new Date().getTime();
  let csrfObj = {
    "sid": req.sessionID,
    "token": CSRF
  }
  // save CSRF token inside a Array
  CSRF_bucket.push(csrfObj);
  console.log(CSRF_bucket);
  res.send(true)
})

app.post('/login', (req, res) => {
  //check reponce body is empty or not
  if (Object.keys(req.body).length === 0 && req.body.constructor === Object) {
    res.statusCode = 400; //bad request
    res.send({
      "authenticationStatus": false,
      "message": "Parse username and password"
    });
  } else {

    //assume username -> abc
    // assume password -> 123

    // if user enter correct credentials
    if (req.body.username == 'abc@gmail.com' && req.body.password == '123') {
      //initialize Cookie NPM module  
      var cookies = new Cookies(req, res, { keys: keys });
      // genrate cookie with sission ID
      cookies.set('sessionID', req.sessionID, {  });
      //geanrate cookie with username
      cookies.set('username', req.body.username, {  });
      //genarate CSRD Token
      let CSRF = new Date().getTime();
      let csrfObj = {
        "sid": req.sessionID,
        "token": CSRF
      }
      // save CSRF token inside a Array
      CSRF_bucket.push(csrfObj);
      console.log(CSRF_bucket);

      //send sucesss responce
      res.statusCode = 200;
      res.send({ "authenticationStatus": true });
    } else {
      res.statusCode = 403; //unotherized
      res.send({
        "authenticationStatus": false,
        "message": "Enter correct username and password"
      });
    }
  }
})

// check the session id is present in the token store
app.post('/csrf', (req, res) => {
  //check reponce body is empty or not
  if (Object.keys(req.body).length === 0 && req.body.constructor === Object) {
    res.statusCode = 400; //bad request
    res.send({
      "message": "Parse sessionId"
    });
  } else {
    let sID = req.body.sessionID;
    // console.log(req.body);
    // console.log(CSRF_bucket);
    let token = "";
    for (let csrfObj of CSRF_bucket) {
      if (csrfObj.sid === sID) {
        token = csrfObj.token
      }
    }
    if(token != ""){
      res.statusCode=200 //sucess
      res.send({ "csrf_token": token });
    }else{
      res.statusCode=403 //Forbidden
      res.send({  
        "message": "login to the service" 
      });
    }
  }
})



app.listen(PORT, () => {
  console.log("Server runs on port : " + PORT);
});
