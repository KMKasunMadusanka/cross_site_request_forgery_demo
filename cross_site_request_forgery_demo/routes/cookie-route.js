const express = require("express");
const bodyParser = require("body-parser");
const cookieSession = require("cookie-session");
const csurf = require("csurf");
const cookieParser = require("cookie-parser");


const router = new express.Router();

router.get("/", function(req, res, next) {
    // Update views
    req.session.views = (req.session.views || 0) + 1;
  
    res.send(`
      <h1>System Login</h1>
      <form action="/messagePage" method="GET">
        <div>
          <label for="username">User Name</label>
          <input id="username" name="username" type="text" />
        </div>
        <div>
          <label for="password">password</label>
          <input id="password" name="password" type="password" />
        </div>
        <input type="submit" value="Submit" />
      </form>
    `);
  
  });
  
  // router.post('/validLogin', (req,res)=>{
  //   if (req.username=='abc' && req.password == '123'){
      
  //   }
  // });
  
  router.get("/messagePage", function(req, res) {
    // Update views
    req.session.views = (req.session.views || 0) + 1;
  
    res.send(`
      <h1>Hello World</h1>
      <form action="/entry" method="POST">
        <div>
          <label for="message">Enter a your name</label>
          <input id="message" name="message" type="text" />
        </div>
        <input type="submit" value="Submit" />
        <input type="hidden" name="_csrf" value="${req.csrfToken()}" />
      </form>
    `);
  
  });
  
  router.post("/entry", (req, res) => {
    console.log(req.cookies._csrf);
    if(req.cookies._csrf == req.body._csrf){
      alert("Hello! I am an alert box!!");
    }
      console.log(`Message received: ${req.body.message}`);
      res.send(`CSRF token used: ${req.body._csrf}, Message received: ${req.body.message}`);
  });
  
  

  module.exports = router