const express = require('express');
const routers = new express.Router();

const cookies = require('./cookie-route.js');

routers.use('/cookie',cookies);

module.exports = routers;