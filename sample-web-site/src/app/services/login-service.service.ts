import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  constructor(private http: HttpClient) { }
  loginUrl = 'http://localhost:5000/login/';

  getLoginStatus(credentials:any) {
    return this.http.post(this.loginUrl,credentials);
  }
}
