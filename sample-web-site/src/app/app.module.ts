import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule }   from '@angular/forms';
import { Routes,RouterModule } from '@angular/router';
import { FormComponent,LoginComponent } from './components'
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

let routes:Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'form', component: FormComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule 
  ],
  providers: [
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
