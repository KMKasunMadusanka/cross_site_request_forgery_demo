import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginServiceService } from '../../services/login-service.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,
              private _LoginServiceService:LoginServiceService) { }
  
    username:any;
    password:any;
  
  ngOnInit() {
  }

  onSubmit( ) {

    let loginObj = {
      "username":this.username,
      "password" : this.password
      }

    this._LoginServiceService.getLoginStatus(loginObj)
    .subscribe((data) => {
      console.log(data);
     // this.router.navigate(['/form']);
    });
   }

}
